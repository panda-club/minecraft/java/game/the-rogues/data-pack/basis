scoreboard objectives setdisplay sidebar
scoreboard players set @a Leave 1
scoreboard players set @a class 1
tag @a remove ranchar
scoreboard players set @e[scores={map=0..}] mapnumber 1
function rogues-basis:game/menu/select_battlefield
time set night

#version sign
setblock -101 41 64 birch_sign[rotation=10]{Text2:'{"text":"Version 2.0.0","color":"black","bold":true,"clickEvent":{"action":"run_command","value":"/tag @s add notesclick"}}',Text3:'{"text":"Click for note","italic":true}'} destroy