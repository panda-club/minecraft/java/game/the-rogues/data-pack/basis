tag @s remove bool
execute as @s at @s positioned ~.3 ~1.82 ~.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~.3 ~1.82 ~-.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~-.3 ~1.82 ~.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~-.3 ~1.82 ~-.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~.3 ~1.2 ~.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~.3 ~1.2 ~-.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~-.3 ~1.2 ~.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~-.3 ~1.2 ~-.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~.3 ~.6 ~.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~.3 ~.6 ~-.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~-.3 ~.6 ~.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~-.3 ~.6 ~-.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~.3 ~ ~.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~.3 ~ ~-.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~-.3 ~ ~.3 run function rogues-basis:do_something
execute as @s[tag=in] at @s positioned ~-.3 ~ ~-.3 run function rogues-basis:do_something
tag @s[tag=in] add bool