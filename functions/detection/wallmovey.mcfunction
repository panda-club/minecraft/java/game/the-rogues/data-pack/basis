execute as @s[scores={dy=1..}] at @s run function rogues-basis:detection/moveyhigh
execute as @s[scores={dy=..-1}] at @s run function rogues-basis:detection/moveylow

scoreboard players remove @s[scores={dy=1..}] dy 1
scoreboard players add @s[scores={dy=..-1}] dy 1

execute as @s[scores={dy=1..}] at @s run function rogues-basis:detection/wallmovey
execute as @s[scores={dy=..-1}] at @s run function rogues-basis:detection/wallmovey